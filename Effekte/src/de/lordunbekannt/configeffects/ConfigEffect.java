package de.lordunbekannt.configeffects;

import java.util.HashMap;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import de.lordunbekannt.effekt.Effekt;
import de.lordunbekannt.lang.LangManager;
import de.lordunbekannt.main.Utils;
import net.minecraft.server.v1_8_R1.EnumParticle;

public class ConfigEffect extends Effekt {

	public ConfigEffect(String name, ItemStack is, int inventorylocation, HashMap<String, String> options) {
		super(name, is, inventorylocation, options);

		new BukkitRunnable() {

			@Override
			public void run() {
				for (Player p : players) {
					EnumParticle particle = EnumParticle.valueOf(getOption("particle"));
					float speed = Float.parseFloat(getOption("speed"));
					int amount = Integer.parseInt(getOption("amount"));
					float spreadx = Float.parseFloat(getOption("spreadx"));
					float spready = Float.parseFloat(getOption("spready"));
					float spreadz = Float.parseFloat(getOption("spreadz"));
					float offsetx = Float.parseFloat(getOption("offsetx"));
					float offsety = Float.parseFloat(getOption("offsety"));
					float offsetz = Float.parseFloat(getOption("offsetz"));

					Particle(particle, p.getLocation().add(offsetx, offsety, offsetz), amount, speed, spreadx, spready,
							spreadz);
				}

			}
		}.runTaskTimer(Utils.getMain(), 0, Integer.parseInt(getOption("repeat")));
	}

}
