package de.lordunbekannt.main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import de.lordunbekannt.configeffects.ConfigEffect;
import de.lordunbekannt.effekt.Effekt;
import de.lordunbekannt.effekt.EffektCommand;
import de.lordunbekannt.effekt.EffektInventory;
import de.lordunbekannt.effekt.EffektManager;
import de.lordunbekannt.effekt.Effekt_Helix;
import de.lordunbekannt.effekt.Effekt_SpreadLove;
import de.lordunbekannt.lang.LangManager;

public class Effekte extends JavaPlugin {

	public static Plugin main;

	@Override
	public void onEnable() {

		main = this;
		loadConfig();
		LangManager.load();
		loadEffects();
		loadCommands();
		loadListeners();
		super.onEnable();
	}

	private void loadCommands() {
		this.getCommand("effect").setExecutor(new EffektCommand());
	}

	private void loadListeners() {
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new EffektInventory(), this);
	}

	private void loadEffects() {

		{
			ItemStack is = new ItemStack(Material.REDSTONE);
			ItemMeta im = is.getItemMeta();
			im.setDisplayName("&eHelix");
			List<String> list = new ArrayList<>();
			list.add("&5Hello~");
			im.setLore(list);
			is.setItemMeta(im);
			HashMap<String, String> hashmap = new HashMap<String, String>();
			hashmap.put("effect", "REDSTONE");
			hashmap.put("repeat", "5");
			new Effekt_Helix("Helix", is, 1, hashmap);

		}

		{
			ItemStack is = new ItemStack(Material.POTION, 1, (short) 8193);
			ItemMeta im = is.getItemMeta();
			im.setDisplayName("&dSpreadlove");
			List<String> list = new ArrayList<>();
			list.add("&7&oVerteilt Liebe <3");
			im.setLore(list);
			is.setItemMeta(im);
			HashMap<String, String> hashmap = new HashMap<String, String>();
			hashmap.put("effect", "HEART");
			hashmap.put("explosioneffect", "LAVA");
			hashmap.put("effectamount", "10");
			hashmap.put("explosioneffectamount", "10");
			hashmap.put("radiusx", "5");
			hashmap.put("radiusy", "5");
			hashmap.put("radiusz", "5");
			hashmap.put("repeat", "" + 20 * 2);
			hashmap.put("enablesound", "true");
			new Effekt_SpreadLove("SpreadLove", is, 0, hashmap);

		}

		{
			ItemStack is = new ItemStack(Material.FIREBALL);
			ItemMeta im = is.getItemMeta();
			im.setDisplayName("&cFeuer");
			List<String> list = new ArrayList<>();
			list.add("&c&lBrennt");
			im.setLore(list);
			is.setItemMeta(im);
			HashMap<String, String> hashmap = new HashMap<String, String>();
			hashmap.put("particle", "FLAME");
			hashmap.put("speed", "0.0");
			hashmap.put("amount", "10");
			hashmap.put("spreadx", "1");
			hashmap.put("spready", "1");
			hashmap.put("spreadz", "1");
			hashmap.put("offsetx", "0");
			hashmap.put("offsety", "0");
			hashmap.put("offsetz", "0");
			hashmap.put("repeat", "10");

			new ConfigEffect("fire", is, 9, hashmap);

		}

		for (String s : LangManager.langconfig.getConfigurationSection("items").getKeys(false)) {
			boolean inside = false;
			for (Effekt eff : EffektManager.name_effekt.values()) {
				if (s.equalsIgnoreCase(eff.name)) {
					inside = true;
				}
			}
			if (inside == false) {
				{
					ItemStack is = new ItemStack(Material.STONE);
					ItemMeta im = is.getItemMeta();
					im.setDisplayName("&cTest");
					List<String> list = new ArrayList<>();
					list.add("&c&lTest");
					im.setLore(list);
					is.setItemMeta(im);
					HashMap<String, String> hashmap = new HashMap<String, String>();
					hashmap.put("particle", "FLAME");
					hashmap.put("speed", "0.0");
					hashmap.put("amount", "10");
					hashmap.put("spreadx", "1");
					hashmap.put("spready", "1");
					hashmap.put("spreadz", "1");
					hashmap.put("offsetx", "0");
					hashmap.put("offsety", "0");
					hashmap.put("offsetz", "0");
					hashmap.put("repeat", "10");

					new ConfigEffect(s, is, 0, hashmap);

				}
			}

		}

	}

	private void loadConfig() {

		{
			FileConfiguration cfg = this.getConfig();
			cfg.addDefault("options.lang", "de");
			cfg.options().copyDefaults(true);
			saveConfig();
		}
		{
			File f = new File(this.getDataFolder() + File.separator + "langs", "de.yml");
			if (!f.exists()) {
				File g = new File(this.getDataFolder() + File.separator + "langs");
				g.mkdirs();

				try {
					f.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			FileConfiguration cfg = YamlConfiguration.loadConfiguration(f);
			// cfg.addDefault("strings.", "");
			cfg.addDefault("strings.lang.name", "&cDeutsch");
			cfg.addDefault("strings.inventory.name", "&5Effekte");
			cfg.addDefault("strings.inventory.isactive", "&a&lAktiv");
			cfg.addDefault("strings.effect.activate", "&7Aktiviere &e%name%");
			cfg.addDefault("strings.inventory.size", 9 * 3);
			// cfg.addDefault("strings.command.", "");
			cfg.addDefault("strings.command.permission", "Effekte.UseCommand");
			cfg.addDefault("strings.command.nopermission", "&cNicht genug Rechte.");

			cfg.options().copyDefaults(true);
			try {
				cfg.save(f);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
