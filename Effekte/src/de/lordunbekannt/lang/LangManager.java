package de.lordunbekannt.lang;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import de.lordunbekannt.main.Utils;

public class LangManager {

	public static HashMap<String, String> shade = new HashMap<>();
	public static HashMap<String, List<String>> listshade = new HashMap<>();
	static String lang;
	public static File langfile;
	public static FileConfiguration langconfig;

	public static void load() {
		lang = Utils.getConfig().getString("options.lang");
		langfile = new File(Utils.getMain().getDataFolder() + File.separator + "langs", lang + ".yml");
		if (!langfile.exists()) {
			System.out.println("[Effekte] Plugin kann die Datei: " + lang + " nicht finden, nutze 'de'.");
			lang = "de";
			load();
			return;
		}
		langconfig = YamlConfiguration.loadConfiguration(langfile);

	}

	public static String getKey(String key) {
		if (shade.containsKey(key))
			return shade.get(key);
		shade.put(key, ChatColor.translateAlternateColorCodes('&', langconfig.getString(key)));
		return ChatColor.translateAlternateColorCodes('&', langconfig.getString(key));
	}

	public static List<String> getListKey(String key) {
		if (shade.containsKey(key))
			return listshade.get(key);
		listshade.put(key, langconfig.getStringList(key));
		return langconfig.getStringList(key);
	}

	public static void saveLangConfig() {
		langconfig.options().copyDefaults(true);
		try {
			langconfig.save(langfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
