package de.lordunbekannt.effekt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import de.lordunbekannt.main.Utils;
import net.minecraft.server.v1_8_R1.EnumParticle;

public class Effekt_SpreadLove extends Effekt {

	ArrayList<Player> targettting = new ArrayList<>();

	public Effekt_SpreadLove(String name, ItemStack is, int inventorylocation, HashMap<String, String> options) {
		super(name, is, inventorylocation, options);

		if (!isEnabled())
			return;

		new BukkitRunnable() {

			@Override
			public void run() {
				for (Player p : players) {
					if (p.getNearbyEntities(Integer.parseInt(getOption("radiusx")),
							Integer.parseInt(getOption("radiusy")), Integer.parseInt(getOption("radiusz")))
							.size() != 0) {
						if (!targettting.contains(p)) {
							int ent = new Random().nextInt(p.getNearbyEntities(Integer.parseInt(getOption("radiusx")),
									Integer.parseInt(getOption("radiusy")), Integer.parseInt(getOption("radiusz")))
									.size());
							if (ent != 0)
								ent--;
							ParticleFromToEntity(p, p.getLocation(),
									p.getNearbyEntities(Integer.parseInt(getOption("radiusx")),
											Integer.parseInt(getOption("radiusy")),
											Integer.parseInt(getOption("radiusz"))).get(ent),
									EnumParticle.valueOf(getOption("effect")), 0.2);
						}
					}

				}

			}
		}.runTaskTimer(Utils.getMain(), 0, Integer.parseInt(getOption("repeat")));
	}

	public void ParticleFromToEntity(Player p, Location from, Entity target, EnumParticle particle, double speed) {

		new BukkitRunnable() {
			double t = 0;

			@Override
			public void run() {
				double reichweite = from.distance(target.getLocation());
				Vector v = target.getLocation().toVector().subtract(from.toVector());
				v.normalize();
				t += speed;

				double x = v.getX() * t;
				double y = v.getY() * t + 1.5;
				double z = v.getZ() * t;
				from.add(x, y, z);
				Particle(particle, from, Integer.parseInt(getOption("effectamount")));
				from.subtract(x, y, z);

				if (t > reichweite) {
					this.cancel();
					Particle(EnumParticle.valueOf(getOption("explosioneffect")), target.getLocation(),
							Integer.parseInt(getOption("explosioneffectamount")));
					if (getOption("enablesound").equalsIgnoreCase("true")) {
						p.playSound(target.getLocation(), Sound.CAT_MEOW, 1, 1);
					}

					if (targettting.contains(p)) {
						targettting.remove(p);
					}

				}

			}
		}.runTaskTimer(Utils.getMain(), 0, 0);

	}
}
