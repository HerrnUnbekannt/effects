package de.lordunbekannt.effekt;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import de.lordunbekannt.main.Utils;
import net.minecraft.server.v1_8_R1.EnumParticle;

public class Effekt_Helix extends Effekt {

	ArrayList<Player> inside = new ArrayList<>();

	public Effekt_Helix(String name, ItemStack is, int inventorylocation, HashMap<String, String> options) {
		super(name, is, inventorylocation, options);

		if (!isEnabled)
			return;

		new BukkitRunnable() {

			@Override
			public void run() {

				for (Player p : players) {

					if (!inside.contains(p)) {
						effect(p);
						inside.add(p);
					}

				}

			}
		}.runTaskTimer(Utils.getMain(), 0, 20 * 1);
	}

	public void effect(Player p) {
		BukkitTask task = new BukkitRunnable() {
			double phi = 0;

			@Override
			public void run() {
				phi += Math.PI / 8;
				double x, y, z;
				Location loc = p.getLocation();

				for (double t = 0; t <= 2 * Math.PI; t += Math.PI / 16) {
					for (double i = 0; i <= 1; i += 1) {
						x = 0.3 * (2 * Math.PI - t) * 0.5 * Math.cos(t + phi + i * Math.PI);
						y = 0.5 * t;
						z = 0.3 * (2 * Math.PI - t) * 0.5 * Math.sin(t + phi + i * Math.PI);
						loc.add(x, y, z);
						Particle(EnumParticle.valueOf(getOption("effect")), loc, 1);
						loc.subtract(x, y, z);
					}
				}
				if (!players.contains(p)) {
					this.cancel();
					if (inside.contains(p))
						inside.remove(p);
				}

			}
		}.runTaskTimer(Utils.getMain(), 0, Integer.parseInt(getOption("repeat")));

	}

}
