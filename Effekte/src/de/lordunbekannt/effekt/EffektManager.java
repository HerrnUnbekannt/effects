package de.lordunbekannt.effekt;

import java.util.HashMap;

import org.bukkit.entity.Player;

public class EffektManager {

	static HashMap<String, String> name_displayname = new HashMap<>();
	static HashMap<String, String> displayname_name = new HashMap<>();
	static HashMap<Player, String> player_name = new HashMap<>();
	public static HashMap<String, Effekt> name_effekt = new HashMap<>();

	public static void addEffekt(Effekt effekt) {
		name_displayname.put(effekt.name, effekt.displayname);
		displayname_name.put(effekt.displayname, effekt.name);
		name_effekt.put(effekt.name, effekt);
	}

	public static Effekt getEffectByDisplayName(String displayname) {
		return name_effekt.get(displayname_name.get(displayname));
	}

	public static Effekt getEffectByName(String name) {
		return name_effekt.get(name);
	}

	public static String getEffektNameByDisplayName(String displayname) {
		return displayname_name.get(displayname);
	}

	public static String getEffektDisplayNameByName(String name) {
		return displayname_name.get(name);
	}

	public static boolean isEffektActive(Player p, String s) {
		if (!player_name.containsKey(p))
			player_name.put(p, "");
		return player_name.get(p).equalsIgnoreCase(s);
	}

	public static Effekt getActiveEffekt(Player p) {
		if (!player_name.containsKey(p))
			return null;
		if (player_name.get(p).equalsIgnoreCase(""))
			return null;
		return getEffectByName(player_name.get(p));
	}

}
