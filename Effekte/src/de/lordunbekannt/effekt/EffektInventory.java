package de.lordunbekannt.effekt;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R1.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.lordunbekannt.lang.LangManager;
import net.minecraft.server.v1_8_R1.NBTTagCompound;
import net.minecraft.server.v1_8_R1.NBTTagList;

public class EffektInventory implements Listener {

	public static void open(Player p) {
		Inventory inv = Bukkit.createInventory(p, Integer.parseInt(LangManager.getKey("strings.inventory.size")),
				LangManager.getKey("strings.inventory.name"));
		for (Effekt effekt : EffektManager.name_effekt.values()) {
			if (effekt.isEnabled) {
				ItemStack is = effekt.itemstack.clone();
				ItemMeta im = is.getItemMeta();
				List<String> lore = im.getLore();
				if (lore == null) {
					lore = new ArrayList<>();
				}
				if (EffektManager.isEffektActive(p, effekt.name)) {
					lore.add(LangManager.getKey("strings.inventory.isactive"));
					im.addEnchant(Enchantment.DURABILITY, 1, true);
					im.setLore(lore);
					is.setItemMeta(im);
					inv.setItem(effekt.location, addGlow(is));
				} else {
					im.setLore(lore);
					is.setItemMeta(im);
					inv.setItem(effekt.location, is);
				}

			}
		}

		p.openInventory(inv);
	}

	public static ItemStack addGlow(ItemStack item) {
		net.minecraft.server.v1_8_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
		NBTTagCompound tag = null;
		if (!nmsStack.hasTag()) {
			tag = new NBTTagCompound();
			nmsStack.setTag(tag);
		}
		if (tag == null)
			tag = nmsStack.getTag();
		NBTTagList ench = new NBTTagList();
		tag.set("ench", ench);
		nmsStack.setTag(tag);
		return CraftItemStack.asCraftMirror(nmsStack);
	}

	@EventHandler
	public void pinvclick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();

		if (e.getInventory().getName().equalsIgnoreCase(LangManager.getKey("strings.inventory.name"))) {
			e.setCancelled(true);
			if (e.getCurrentItem() != null) {
				if (e.getCurrentItem().getType() != Material.AIR) {
					if (e.getCurrentItem().getItemMeta().getDisplayName() != null) {
						String name = e.getCurrentItem().getItemMeta().getDisplayName();
						if (EffektManager.displayname_name.containsKey(name)) {
							p.closeInventory();
							if (EffektManager.getActiveEffekt(p) != null) {
								EffektManager.getActiveEffekt(p).removePlayer(p);
							}
							p.sendMessage(LangManager.getKey("strings.effect.activate").replaceAll("%name%",
									EffektManager.getEffectByDisplayName(name).name));
							EffektManager.player_name.put(p, EffektManager.getEffectByDisplayName(name).name);
							EffektManager.getEffectByDisplayName(name).addPlayer(p);
						}
					}
				}
			}
		}
	}

}
