package de.lordunbekannt.effekt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.lordunbekannt.lang.LangManager;
import net.minecraft.server.v1_8_R1.EnumParticle;
import net.minecraft.server.v1_8_R1.PacketPlayOutWorldParticles;

public class Effekt {

	public ArrayList<Player> players = new ArrayList<>();

	public String name;
	public String displayname;

	public int location;
	public ItemStack defaultitemstack;
	public ItemStack itemstack;
	public boolean isEnabled = false;
	HashMap<String, String> options = new HashMap<>();

	public Effekt(String name, ItemStack is, int inventorylocation, HashMap<String, String> options) {
		this.name = name;
		this.options = options;
		{
			FileConfiguration cfg = LangManager.langconfig;
			cfg.addDefault("items." + name + ".enabled", true);
			// cfg.addDefault("items." + name + ".", "");
			cfg.addDefault("items." + name + ".itemstack.displayname", is.getItemMeta().getDisplayName());
			cfg.addDefault("items." + name + ".itemstack.amount", is.getAmount());
			cfg.addDefault("items." + name + ".itemstack.materialid", is.getTypeId());
			cfg.addDefault("items." + name + ".itemstack.data", is.getDurability());
			cfg.addDefault("items." + name + ".itemstack.lore", is.getItemMeta().getLore());
			cfg.addDefault("items." + name + ".inventory.location", inventorylocation);

			for (String key : options.keySet()) {
				String value = options.get(key);
				cfg.addDefault("items." + name + ".itemstack.options." + key, value);
			}
			LangManager.saveLangConfig();
			LangManager.load();

			HashMap<String, String> hash = new HashMap<>();
			if (cfg.contains("items." + name + ".itemstack.options")) {
				for (String s : cfg.getConfigurationSection("items." + name + ".itemstack.options").getKeys(false)) {
					hash.put(s, cfg.getString("items." + name + ".itemstack.options." + s));
				}
			}
			this.options = hash;
		}
		{
			location = Integer.parseInt(LangManager.getKey("items." + name + ".inventory.location"));
			isEnabled = Boolean.parseBoolean(LangManager.getKey("items." + name + ".enabled"));
			displayname = LangManager.getKey("items." + name + ".itemstack.displayname");
			itemstack = StringToItemStack(displayname,
					Integer.parseInt(LangManager.getKey("items." + name + ".itemstack.amount")),
					Integer.parseInt(LangManager.getKey("items." + name + ".itemstack.materialid")),
					Integer.parseInt(LangManager.getKey("items." + name + ".itemstack.data")),
					LangManager.getListKey("items." + name + ".itemstack.lore"));
		}

		EffektManager.addEffekt(this);
	}

	public boolean isEnabled() {
		return isEnabled;
	}

	public String getOption(String key) {
		return options.get(key);
	}

	@SuppressWarnings({ "unused", "deprecation" })
	private ItemStack StringToItemStack(String name, int amount, int typeid, int data, List<String> lore) {
		ItemStack is = new ItemStack(typeid, amount, (short) data);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(name);
		List<String> l = new ArrayList<>();
		for (String s : lore) {
			l.add(ChatColor.translateAlternateColorCodes('&', s));
		}
		im.setLore(l);
		is.setItemMeta(im);
		return is;
	}

	public void addPlayer(Player p) {
		players.add(p);
	}

	public void removePlayer(Player p) {
		players.remove(p);
	}

	public boolean inside(Player p) {
		return players.contains(p);
	}

	public void Particle(EnumParticle particle, Location loc, int amount) {
		PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(particle, true, (float) loc.getX(),
				(float) loc.getY(), (float) loc.getZ(), 0, 0, 0, 0, amount, null);
		for (Player p : Bukkit.getOnlinePlayers()) {
			((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
		}

	}

	public static void Particle(EnumParticle particle, Location loc, int amount, float speed) {
		PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(particle, true, (float) loc.getX(),
				(float) loc.getY(), (float) loc.getZ(), 0, 0, 0, speed, amount, null);
		for (Player p : Bukkit.getOnlinePlayers()) {
			((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
		}

	}

	public static void Particle(EnumParticle particle, Location loc, int amount, float speed, float spreadx,
			float spready, float spreadz) {
		PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(particle, true, (float) loc.getX(),
				(float) loc.getY(), (float) loc.getZ(), spreadx, spready, spreadz, speed, amount, null);
		for (Player p : Bukkit.getOnlinePlayers()) {
			((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
		}

	}
}
