package de.lordunbekannt.effekt;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.lordunbekannt.lang.LangManager;

public class EffektCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {

		if (cs instanceof Player) {
			Player p = (Player) cs;
			if (p.hasPermission(LangManager.getKey("strings.command.permission"))) {
				EffektInventory.open(p);
			} else {
				p.sendMessage(LangManager.getKey("strings.command.nopermission"));
			}
		}

		return false;
	}

}